Pour que le bug soit traité, merci de préciser quelques détails.

### Environment

> Indiquez quelques éléments de votre installation

* Foundry VTT Version: (Example 0.5.4)
* OS: [Windows, MacOS, Linux (which distro)]
* Modules ?: Liste des modules utilisés

### Description du problème


/label ~Bug ~Nonrepro