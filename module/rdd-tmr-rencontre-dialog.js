/* -------------------------------------------- */
export class RdDTMRRencontreDialog extends Dialog {
  
  /* -------------------------------------------- */
  constructor(html, tmrApp, rencontreData) {
    const dialogConf = {
      title: "Rencontre en TMR!",
      content: "Vous recontrez un " + rencontreData.name + " de force " + rencontreData.force + "<br>",
      buttons: {
        derober: { icon: '<i class="fas fa-check"></i>', label: "Se dérober", callback: () => { this.toClose = true; this.tmrApp.derober() } },
        refouler: { icon: '<i class="fas fa-check"></i>', label: "Refouler", callback: () => { this.toClose = true;this.tmrApp.refouler() } },
        maitiser: { icon: '<i class="fas fa-check"></i>', label: "Maîtriser", callback: () => { this.toClose = true;this.tmrApp.maitriser() } }
      },
      default: "derober"
    }
  
    const dialogOptions = {
      classes: ["tmrrencdialog"],
      width: 320, height: 240,
      'z-index': 20
    }
    super(dialogConf, dialogOptions);

    this.toClose = false;
    this.rencontreData = duplicate(rencontreData);
    this.tmrApp = tmrApp;
    this.tmrApp.minimize();
  }
  
  /* -------------------------------------------- */
  close() {
    if ( this.toClose ) {
      this.tmrApp.maximize();
      return super.close();
    }
    ui.notifications.info("Vous devez résoudre la rencontre.");
  }

}
