/**
 * Extend the base Dialog entity by defining a custom window to perform roll.
 * @extends {Dialog}
 */
export class RdDEncaisser extends Dialog {

  /* -------------------------------------------- */
  constructor(html, actor) {
    // Common conf
    let dialogConf = {
      title: "Jet d'Encaissement",
      content: html,
      buttons: {
        "mortel": { label: "mortel", callback: html => this.performEncaisser(html, "mortel") },
        "non-mortel": { label: "non-mortel", callback: html => this.performEncaisser(html, "non-mortel") },
        "cauchemar": { label: "cauchemar", callback: html => this.performEncaisser(html, "cauchemar") }
      },
      default: "coupMortel"
    }

    let dialogOptions = {
      classes: ["rdddialog"],
      width: 320,
      height: 240
    }

    // Select proper roll dialog template and stuff
    super(dialogConf, dialogOptions);

    this.actor = actor;
    this.modifier = 0;
  }

  /* -------------------------------------------- */
  performEncaisser(html, mortalite = "mortel") {
    const ajustement = Number(this.modifier);
    const encaissement = new Roll("2d10").roll().total + ajustement;
    this.actor.encaisserDommages({
      degats: encaissement,
      domArmePlusDom: ajustement,
      loc: { result: 0, label: "Corps" },
      mortalite: mortalite
    });
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);

    // Setup everything onload
    $(function () {
      $("#modificateurDegats").val("0");
    });

    html.find('#modificateurDegats').change((event) => {
      this.modifier = event.currentTarget.value; // Update the selected bonus/malus
    });
  }

}
