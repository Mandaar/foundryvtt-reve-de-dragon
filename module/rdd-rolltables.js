export class RdDRollTables {

  /* -------------------------------------------- */
  static async genericGetTableResult(tableName, toChat) {
    const pack = game.packs.get("foundryvtt-reve-de-dragon.tables-diverses");
    const index = await pack.getIndex();
    const entry = index.find(e => e.name === tableName);
    const table = await pack.getEntity(entry._id);
    const result = await table.draw({ displayChat: toChat });
    console.log("RdDRollTables", tableName, toChat, ":", result);
    return result;
  }

  /* -------------------------------------------- */
  static async getSouffle(toChat = true) {
    return RdDRollTables.genericGetTableResult("Souffles de Dragon", toChat);
  }

  /* -------------------------------------------- */
  static async getQueue(toChat = true) {
    return RdDRollTables.genericGetTableResult("Queues de dragon", toChat);
  }

  /* -------------------------------------------- */
  static async getTete(toChat = true) {
    return RdDRollTables.genericGetTableResult("Têtes de Dragon pour haut-rêvants", toChat);
  }

  /* -------------------------------------------- */
  static async getTeteHR(toChat = true) {
    return RdDRollTables.genericGetTableResult("Têtes de Dragon pour tous personnages", toChat);
  }

  /* -------------------------------------------- */
  static async getOmbre(toChat = true) {
    return RdDRollTables.genericGetTableResult("Ombre de Thanatos", toChat);
  }

  /* -------------------------------------------- */
  static async getTarot(toChat = true) {
    return RdDRollTables.genericGetTableResult("Tarot Draconique", toChat);
  }
}
