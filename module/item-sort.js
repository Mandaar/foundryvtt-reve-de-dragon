import { Misc } from "./misc.js";

export class RdDItemSort extends Item {

  static isDifficulteVariable(sort) {
    return sort && (sort.data.difficulte.toLowerCase() == "variable");
  }
  static isCoutVariable(sort) {
    return sort && (sort.data.ptreve.toLowerCase() == "variable" || sort.data.ptreve.indexOf("+") >= 0);
  }
 
  static setCoutReveReel(sort){
    if (sort) {
      sort.data.ptreve_reel = this.isCoutVariable(sort) ? 1 : sort.data.ptreve;
    }
  }

  static getDifficulte(sort, variable) {
    if (sort && !RdDItemSort.isDifficulteVariable(sort)) {
       return Misc.toInt(sort.data.difficulte);
    }
    return variable;
  }

}