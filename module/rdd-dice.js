import { ChatUtility } from "./chat-utility.js";

export class RdDDice {

  /* -------------------------------------------- */
  static async deDraconique(rollMode="selfroll") {
    let roll = new Roll("1d8x8").evaluate();
    await this.show(roll, rollMode);
    return roll.total - Math.ceil(roll.total / 8);
  }


  /* -------------------------------------------- */
  static async show(roll, rollMode = undefined) {
    if (roll.showDice || game.settings.get("foundryvtt-reve-de-dragon", "dice-so-nice") == true) {
      await this.showDiceSoNice(roll, rollMode);
    }
    return roll;
  }
  
  /* -------------------------------------------- */
  static async showDiceSoNice(roll, rollMode = undefined) {
    if (game.modules.get("dice-so-nice") && game.modules.get("dice-so-nice").active) {
      let whisper = null;
      let blind = false;
      rollMode = rollMode == undefined ? game.settings.get("core", "rollMode") : rollMode;
      switch (rollMode) {
        case "blindroll": //GM only
          blind = true;
        case "gmroll": //GM + rolling player
          whisper = ChatUtility.getUsers(user => user.isGM);
          break;
        case "roll": //everybody
          whisper = ChatUtility.getUsers(user => user.active);
          break;
        case "selfroll":
          whisper = [game.user._id];
          break;
      }
      await game.dice3d.showForRoll(roll, game.user, true, whisper, blind);
    }
  }
}