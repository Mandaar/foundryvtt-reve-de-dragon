/* Common useful functions shared between objects */

import { TMRUtility } from "./tmr-utility.js";
import { RdDRollTables } from "./rdd-rolltables.js";
import { ChatUtility } from "./chat-utility.js";

const level_category = { 
  "generale": "-4", 
  "particuliere": "-8", 
  "speciale": "-11", 
  "connaissance": "-11", 
  "draconic": "-11", 
  "melee": "-6", 
  "tir": "-8", 
  "lancer": "-8"
}
const competenceTroncs = [ ["Esquive", "Dague", "Corps à corps"],
                           ["Epée à 1 main", "Epée à 2 mains", "Hache à 1 main", "Hache à 2 mains", "Lance", "Masse à 1 main", "Masse à 2 mains"] ];
const competence_xp = {
  "-11" : [ 5, 10, 15, 25, 35, 45, 55, 70, 85, 100, 115, 135, 155, 175 ],
  "-8"  : [ 10, 20, 30, 40, 55, 70, 85, 100, 120, 140,160],
  "-6"  : [ 10, 20, 35, 50, 65, 80, 100, 120, 140],
  "-4"  : [ 15, 30, 45, 60, 80, 100, 120]
}

// This table starts at 0 -> niveau -10
const competence_xp_par_niveau = [ 5, 5, 5, 10, 10, 10, 10, 15, 15, 15, 15, 20, 20, 20, 20, 30, 30, 40, 40, 60, 60, 100, 100, 100, 100, 100, 100, 100, 100, 100];
const carac_array = [ "taille", "apparence", "constitution", "force", "agilite", "dexterite", "vue", "ouie", "odoratgout", "volonte", "intellect", "empathie", "reve", "chance", "melee", "tir", "lancer", "derobee"];
const difficultesLibres  = [0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10];
const ajustementsConditions  = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5, +6, +7, +8, +9, +10];
const ajustementsEncaissement  = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5, +6, +7, +8, +9, +10, +11, +12, +13, +14, +15, +16, +17, +18, +19, +20, +21, +22, +23, +24, +25];


function _buildAllSegmentsFatigue(max) {
  const cycle = [5, 2, 4, 1, 3, 0];
  let fatigue = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];
  for (let i = 0; i <= max; i++) {
    const ligneFatigue= duplicate(fatigue[i]);
    const caseIncrementee = cycle[i % 6];
    ligneFatigue[caseIncrementee]++;
    ligneFatigue[caseIncrementee + 6]++;
    ligneFatigue.fatigueMax = 2 * (i + 1);
    fatigue[i + 1] = ligneFatigue ;

  }
  return fatigue;
}
function _cumulSegmentsFatigue(matrix) {
  let cumulMatrix = [];
  for (let line of matrix)
  {
    let cumul = duplicate(line);
    
    for (let i = 1; i < 12; i++) {
      cumul[i] += cumul[i - 1];
    }
    cumulMatrix.push(cumul);
  }
  return cumulMatrix;
}

const fatigueMatrix = _buildAllSegmentsFatigue(30);
const cumulFatigueMatrix = _cumulSegmentsFatigue(fatigueMatrix);

const fatigueMalus = [ 0, 0, 0, -1, -1, -1, -2, -3, -4, -5, -6, -7 ]; // Provides the malus for each segment of fatigue
const fatigueLineSize =  [ 3, 6, 7, 8, 9, 10, 11, 12];
const fatigueLineMalus = [ 0, -1, -2, -3, -4, -5, -6, -7 ];
const fatigueMarche = { "aise":      { "4":1, "6":2, "8":3, "10":4, "12":6 },
                        "malaise":   { "4":2, "6":3, "8":4, "10":6 },
                        "difficile": { "4":3, "6":4, "8":6 },
                        "tresdifficile": { "4":4, "6":6 } }
/* Static tables for commands /table */
const table2func = { "queues":  {descr: "queues : Tire une queue de Dragon", func: RdDRollTables.getQueue}, 
                     "ombre":   { descr: "ombre: Tire une Ombre de Dragon", func: RdDRollTables.getOmbre }, 
                     "tetehr":  {descr: "tetehr: Tire une Tête de Dragon pour Hauts Revants", fund: RdDRollTables.getTeteHR},
                     "tete"  :  { descr: "tete: Tire une Tête de Dragon", func: RdDRollTables.getTete},
                     "souffle": { descr: "souffle: Tire un Souffle de Dragon", func: RdDRollTables.getSouffle},
                     "tarot"  : { descr: "tarot: Tire une carte de Tarot Dracnique", func: RdDRollTables.getTarot} };

const definitionsBlessures = [
  { type: "legere", facteur: 2 },
  { type: "grave", facteur : 4 },
  { type: "critique", facteur : 6 }
]

const definitionsEncaissement = {
  "mortel": [
    { minimum: undefined, maximum: 0, endurance: "0", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 1, maximum: 10, endurance: "1d4", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 11, maximum: 15, endurance: "1d6", vie: "0", legeres: 1, graves: 0, critiques: 0 },
    { minimum: 16, maximum: 19, endurance: "2d6", vie: "2", legeres: 0, graves: 1, critiques: 0 },
    { minimum: 20, maximum: undefined, endurance: "100", vie: "4 + @over20", legeres: 0, graves: 0, critiques: 1 },
  ],
  "non-mortel": [
    { minimum: undefined, maximum: 0, endurance: "0", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 1, maximum: 10, endurance: "1d4", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 11, maximum: 15, endurance: "1d6", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 16, maximum: 19, endurance: "2d6", vie: "0", legeres: 1, graves: 0, critiques: 0 },
    { minimum: 20, maximum: undefined, endurance: "100", vie: "1", legeres: 1, graves: 0, critiques: 0 },
  ],
  "cauchemar": [
    { minimum: undefined, maximum: 0, endurance: "0", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 1, maximum: 10, endurance: "1d4", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 11, maximum: 15, endurance: "1d6", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 16, maximum: 19, endurance: "2d6", vie: "0", legeres: 0, graves: 0, critiques: 0 },
    { minimum: 20, maximum: undefined, endurance: "3d6 + @over20", vie: "0", legeres: 0, graves: 0, critiques: 0 },
  ]
};

/* -------------------------------------------- */
export class RdDUtility  {
  /* -------------------------------------------- */
  static async preloadHandlebarsTemplates( ) {
    const templatePaths = [
      //Character Sheets
      'systems/foundryvtt-reve-de-dragon/templates/actor-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-creature-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-humanoide-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/actor-entite-sheet.html',
      //Items
      'systems/foundryvtt-reve-de-dragon/templates/item-competence-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-competencecreature-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-arme-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-armure-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-objet-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-conteneur-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-sort-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-herbe-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-ingredient-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-livre-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-tache-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-potion-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-rencontresTMR-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-queue-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-souffle-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-tarot-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-tete-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/item-ombre-sheet.html',
      'systems/foundryvtt-reve-de-dragon/templates/competence-categorie.html',
      'systems/foundryvtt-reve-de-dragon/templates/competence-carac-defaut.html',
      'systems/foundryvtt-reve-de-dragon/templates/competence-base.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-aspect-tarot.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-categorie.html',
      'systems/foundryvtt-reve-de-dragon/templates/enum-rarete.html',
      'systems/foundryvtt-reve-de-dragon/templates/arme-competence.html',
      'systems/foundryvtt-reve-de-dragon/templates/sort-draconic.html',
      'systems/foundryvtt-reve-de-dragon/templates/sort-tmr.html',
      // Dialogs
      'systems/foundryvtt-reve-de-dragon/templates/dialog-competence.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-carac.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-sort.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-encaisser.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-tmr.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-surenc.html',
      'systems/foundryvtt-reve-de-dragon/templates/dialog-roll-natation.html',
      // Calendrier
      'systems/foundryvtt-reve-de-dragon/templates/calendar-template.html',
      // Conteneur/item in Actor sheet
      'systems/foundryvtt-reve-de-dragon/templates/actor-inventaire-conteneur.html'
    ];

    return loadTemplates(templatePaths);
  }

  /* -------------------------------------------- */  
  static checkNull(items) {
    if (items && items.length) {
      return items;
    }
    return [];
  }

  /* -------------------------------------------- */
  static filterItemsPerTypeForSheet( data ) {
    data.data.materiel    = this.checkNull(data.itemsByType['objet']);
    data.data.conteneurs  = this.checkNull(data.itemsByType['conteneur']);
    data.data.armes       = this.checkNull(data.itemsByType['arme']);
    data.data.armures     = this.checkNull(data.itemsByType['armure']);
    data.data.livres      = this.checkNull(data.itemsByType['livre']);
    data.data.potions     = this.checkNull(data.itemsByType['potion']);
    data.data.ingredients = this.checkNull(data.itemsByType['ingredient']);
    data.data.munitions   = this.checkNull(data.itemsByType['munition']);
    data.data.herbes      = this.checkNull(data.itemsByType['herbe']);
    data.data.sorts       = this.checkNull(data.itemsByType['sort']);
    data.data.queues      = this.checkNull(data.itemsByType['queue']);
    data.data.souffles    = this.checkNull(data.itemsByType['souffle']);
    data.data.ombres      = this.checkNull(data.itemsByType['ombre']);
    data.data.tetes       = this.checkNull(data.itemsByType['tete']);
    data.data.objets      = data.data.conteneurs.concat(data.data.materiel).concat(data.data.armes).concat(data.data.armures).concat(data.data.munitions).concat(data.data.livres).concat(data.data.potions).concat(data.data.herbes).concat(data.data.ingredients);
  }

  /* -------------------------------------------- */
  static async processItemDropEvent( actorSheet, event) {
    let dragData = JSON.parse(event.dataTransfer.getData("text/plain"));
    let dropID = $(event.target).parents(".item").attr("data-item-id"); // Only relevant if container drop
    if ( dropID ) { // Dropped over an item !!!
      let objetId = dragData.id || dragData.data._id;
      if ( actorSheet.objetVersConteneur[objetId] != dropID ) {
        if ( actorSheet.actor.testConteneurCapacite(objetId, dropID) ) { 
          await actorSheet.actor.enleverDeConteneur(objetId, actorSheet.objetVersConteneur[objetId]);
          await actorSheet.actor.ajouterAConteneur(objetId, dropID);
        } else {
          ui.notifications.info("Capacité d'encombrement insuffisante dans le conteneur !");
        }
      }
    }  
    actorSheet.actor.computeEncombrementTotalEtMalusArmure();
  }

  /* -------------------------------------------- */
  static buildArbreDeConteneur( actorSheet, data ) {
    actorSheet.objetVersConteneur = {}; // Table de hash locale pour recupération rapide du conteneur parent (si existant)    
    // Attribution des objets aux conteneurs
    for (let conteneur of data.data.conteneurs) {
      conteneur.subItems = [];
      if (!conteneur.data.encTotal) conteneur.data.encTotal = 0;
      //conteneur.data.encTotal = ; Deja calculé
      if (conteneur.data.contenu) {
        for (let id of conteneur.data.contenu) {
          let objet = data.data.objets.find( objet => (id == objet._id) );
          if (objet) {
            if (!objet.data.encombrement) objet.data.encombrement = 0; // Auto-fix
            objet.estContenu = true; // Permet de filtrer ce qifui est porté dans le template
            actorSheet.objetVersConteneur[id] = conteneur._id;
            conteneur.data.encTotal += Number(objet.data.encombrement) * Number(((objet.data.quantite)?objet.data.quantite:1));
            conteneur.subItems.push( objet );
          }
        }
      }
    }
    // Construit la liste des conteneurs de niveau 1 (c'est à dire non contenu eux-même dans un conteneur)
    let newConteneurs = data.data.conteneurs.filter(function(conteneur, index, arr) { return !conteneur.estContenu } );
    data.data.conteneurs = newConteneurs;
  }

  /* -------------------------------------------- */
  /** Construit la structure récursive des conteneurs, avec imbrication potentielle
   * 
   */
  static buildConteneur( objet, niveau ) {
    if (!niveau) niveau = 1;
    let str = Handlebars.partials['systems/foundryvtt-reve-de-dragon/templates/actor-inventaire-conteneur.html']( { item: objet} );
    if (objet.type == 'conteneur') {
      str = str + "<ul class='item-list alterne-list list-item-margin"+niveau+"'>";
      for (let subItem of objet.subItems) {
        str = str + this.buildConteneur(subItem, niveau+1);
      }
      str = str + "</ul>";
    }
    return new Handlebars.SafeString(str);
  }

  /* -------------------------------------------- */
  static buildResolutionTable( ) {
    let tableRes = []
    for (var j=0; j<=21; j++) {
      let subtab = [];
      for (var i=-10; i<=22; i++) {
        var m = (i + 10) * 0.5;
        var v;
        if (i == -9) {
          v = Math.floor(j / 2);
        } else if (i == -10) {
          v = Math.floor(j / 4);
        } else {
          if (j % 2 == 0) {
            var v = Math.ceil(j * m);
          } else {
            var v = Math.floor(j * m);
          }
        }
        if (v < 1) v = 1;
        let specResults
        if ( v > 100 )
          specResults = { part: Math.ceil(v / 5), epart: 1000, etotal: 1000 };
        else 
           specResults = specialResults[Math.ceil(v / 5 )];        
        let tabIndex = i+10;
        subtab[tabIndex] = { niveau: i, score: v, part: specResults.part, epart: specResults.epart, etotal: specResults.etotal } 
      }
      tableRes[j] = subtab;
    }
    return tableRes;
  }

  /* -------------------------------------------- */
  static getLevelCategory( )  
  {
    return level_category;
  }
  static getCaracArray()
  {
    return carac_array;
  }
  static getDifficultesLibres()
  {
    return difficultesLibres;
  }
  static getAjustementsConditions()
  {
    return ajustementsConditions;
  }
  static getAjustementsEncaissement()
  {
    return ajustementsEncaissement;
  }

  static getDefinitionsBlessures() {
    return definitionsBlessures;
  }
  /* -------------------------------------------- */
  static isTronc( compName )
  {
    for (let troncList of competenceTroncs) {
      for (let troncName of troncList) {
        if ( troncName == compName) 
          return troncList;
      }
    }
    return false;
  }

  /* -------------------------------------------- */
  static computeCompetenceXPCost( competence )
  {
    let minLevel = competence.data.base;
    if ( minLevel == competence.data.niveau) return 0;
    if ( competence.data.niveau < -10) return 0;

    let xp = 0;
    for (let i=minLevel+1; i<=competence.data.niveau; i++) {
       xp += competence_xp_par_niveau[i+10];
       //console.log(i, i+10, competence_xp_par_niveau[i+10]);
    }
    return xp;
  } 

  /* -------------------------------------------- */
  static computeCompetenceTroncXP( competenceList )
  {
    let xp = 0;
    for (let troncList of competenceTroncs) {
      let minNiveau = 0;
      for (let troncName of troncList) {
        let comp = RdDUtility.findCompetence( competenceList, troncName);
        if (comp) {
          minNiveau = Math.min(comp.data.niveau, minNiveau); 
        }
      }
      minNiveau = Math.max(minNiveau, 0); // Clamp à 0, pour le tronc commun
      let minNiveauXP = competence_xp_par_niveau[minNiveau+10];
      xp += minNiveauXP;
      for (let troncName of troncList) {
        let comp = RdDUtility.findCompetence( competenceList, troncName);
        if (comp){
          xp += competence_xp_par_niveau[comp.data.niveau+10] - minNiveauXP;
        } 
      }
    }
    return xp;
  }
  
  /* -------------------------------------------- */
  /** Retourne une liste triée d'armes avec le split arme1 main / arme 2 main */
  static finalizeArmeList( armeList, competenceByCategory ) {
    // Gestion des armes 1/2 mains
    let arme2mains = []; // Tableau contenant la duplication des armes 1m/2m
    for (const arme of armeList) { 
      // Dupliquer les armes pouvant être à 1 main et 2 mains en patchant la compétence
      if (arme.data.unemain && arme.data.deuxmains) {
        let arme2main = duplicate(arme);
        arme2main.data.dommages = arme2main.data.dommages.split("/")[1]; // Existence temporaire uniquement dans la liste des armes, donc OK
        arme2main.data.competence = arme2main.data.competence.replace(" 1 main", " 2 mains"); // Replace !
        for ( const melee of competenceByCategory.melee ) {
          if (melee.name == arme2main.data.competence ) 
            arme2main.data.niveau = melee.data.niveau
        }
        arme2mains.push(arme2main);
      }
    }
    armeList = armeList.concat(arme2mains); // Merge all cases
    armeList = armeList.sort((a, b) =>  { if ( a.name > b.name) return 1; else return -1; } );
    return armeList
  }

  /* -------------------------------------------- */
  static computeCarac( data)
  {
    data.carac.force.value = Math.min(data.carac.force.value, parseInt(data.carac.taille.value) + 4);
        
    data.carac.derobee.value = Math.floor(parseInt(((21 - data.carac.taille.value)) + parseInt(data.carac.agilite.value)) / 2);
    let bonusDomKey = Math.floor( (parseInt(data.carac.force.value) + parseInt(data.carac.taille.value)) / 2);

    // TODO: gérer table des bonus dommages (et autres) des créatures
    data.attributs.plusdom.value = 2
    if (bonusDomKey < 8) 
      data.attributs.plusdom.value = -1;
    else if (bonusDomKey < 12) 
      data.attributs.plusdom.value = 0;
    else if (bonusDomKey < 14) 
      data.attributs.plusdom.value = 1;
      
    data.attributs.encombrement.value = (parseInt(data.carac.force.value) + parseInt(data.carac.taille.value)) / 2;
    data.carac.melee.value = Math.floor( (parseInt(data.carac.force.value) + parseInt(data.carac.agilite.value)) / 2);
    data.carac.tir.value = Math.floor( (parseInt(data.carac.vue.value) + parseInt(data.carac.dexterite.value)) / 2);
    data.carac.lancer.value = Math.floor( (parseInt(data.carac.tir.value) + parseInt(data.carac.force.value)) / 2);
    
    data.sante.vie.max = Math.ceil( (parseInt(data.carac.taille.value) + parseInt(data.carac.constitution.value)) /2 );
    
    data.sante.vie.value = Math.min(data.sante.vie.value, data.sante.vie.max)
    data.sante.endurance.max = Math.max( parseInt(data.carac.taille.value) + parseInt(data.carac.constitution.value), parseInt(data.sante.vie.max) + parseInt(data.carac.volonte.value) );
    data.sante.endurance.value = Math.min(data.sante.endurance.value, data.sante.endurance.max);
    data.sante.fatigue.max   = data.sante.endurance.max*2;
    data.sante.fatigue.value = Math.min(data.sante.fatigue.value, data.sante.fatigue.max);
    
    data.attributs.sconst.value = 5; // Max !
    if ( data.carac.constitution.value < 9 ) 
      data.attributs.sconst.value = 2;
    else if (data.carac.constitution.value < 12 )
      data.attributs.sconst.value = 3;
    else if (data.carac.constitution.value < 15 )
      data.attributs.sconst.value = 4;      
    
    data.attributs.sust.value = 4; // Max !
    if ( data.carac.taille.value < 10 ) 
      data.attributs.sust.value = 2;
    else if (data.carac.taille.value < 14 )
      data.attributs.sust.value = 3;
      
    //Compteurs
    //data.compteurs.reve.value   = data.carac.reve.value;
    data.reve.reve.max = data.carac.reve.value;
    //data.compteurs.chance.value = data.carac.chance.value;
    data.compteurs.chance.max = data.carac.chance.value;
  }

  /* -------------------------------------------- */
  static getSegmentsFatigue(maxEnd) {
    maxEnd = Math.max(maxEnd, 1);
    maxEnd = Math.min(maxEnd, fatigueMatrix.length);
    return fatigueMatrix[maxEnd];
  }

  /* -------------------------------------------- */
  static calculMalusFatigue(fatigue, maxEnd)
  {
    maxEnd = Math.max(maxEnd, 1);
    maxEnd = Math.min(maxEnd, cumulFatigueMatrix.length);
    let segments = cumulFatigueMatrix[maxEnd];
    for (let i=0; i<12; i++) {
      if (fatigue <= segments[i]) {
        return fatigueMalus[i]
      }
    }
    return -7;
  }
  /* -------------------------------------------- */
  // Build the nice (?) html table used to manage fatigue.
  // max should be the endurance max value
  static makeHTMLfatigueMatrix( fatigue, maxEndurance) {
    let segments = this.getSegmentsFatigue(maxEndurance);
    return this.makeHTMLfatigueMatrixForSegment(fatigue, segments);
  }

  static makeHTMLfatigueMatrixForSegment(fatigue, segments) {
    fatigue = Math.max(fatigue, 0);
    fatigue = Math.min(fatigue, segments.fatigueMax);

    let table = $("<table/>").addClass('table-fatigue');
    let segmentIdx = 0;
    let fatigueCount = 0;
    for (var line = 0; line < fatigueLineSize.length; line++) {
      let row = $("<tr/>");
      let segmentsPerLine = fatigueLineSize[line];
      row.append("<td class='fatigue-malus'>" + fatigueLineMalus[line] + "</td>");
      while (segmentIdx < segmentsPerLine) {
        let freeSize = segments[segmentIdx];
        for (let col = 0; col < 5; col++) {
          if (col < freeSize) {
            if (fatigueCount < fatigue)
              row.append("<td class='fatigue-used'>X</td>");


            else
              row.append("<td class='fatigue-free'/>");
            fatigueCount++;
          } else {
            row.append("<td class='fatigue-none'/>");
          }
        }
        row.append("<td class='fatigue-separator'/>");
        segmentIdx = segmentIdx + 1;
      }
      table.append(row);
    }
    return table;
  }

  /* -------------------------------------------- */
  static getLocalisation( ) 
  {    
    let result = new Roll("1d20").roll().total;
    let txt = ""
    if ( result <= 3 )  txt = "Jambe, genou, pied, jarret";
    else if ( result <= 7 )  txt = "Hanche, cuisse, fesse";
    else if ( result <= 9 )  txt = "Ventre, reins";
    else if ( result <= 12 ) txt = "Poitrine, dos";
    else if ( result <= 14 ) txt = "Avant-bras, main, coude";
    else if ( result <= 18 ) txt = "Epaule, bras, omoplate";
    else if ( result == 19)  txt = "Tête";
    else if ( result == 20)  txt = "Tête (visage)";
    
    return { result: result, label: txt };
  }

  /* -------------------------------------------- */
  static computeBlessuresSante( degats, mortalite="mortel" ) {
    let encaissement = RdDUtility.selectEncaissement(degats, mortalite)
    let over20 =  degats > 20 ? degats - 20 : 0 
    encaissement.endurance = - RdDUtility._evaluatePerte(encaissement.endurance, over20)
    encaissement.vie = - RdDUtility._evaluatePerte(encaissement.vie, over20)
    return encaissement;
  }

  /* -------------------------------------------- */
  static selectEncaissement( degats, mortalite ) {
    const table = definitionsEncaissement[mortalite] === undefined ? definitionsEncaissement["mortel"] : definitionsEncaissement[mortalite];
    for (let encaissement of table) {
      if ((encaissement.minimum === undefined || encaissement.minimum <= degats)
        && (encaissement.maximum === undefined || degats <= encaissement.maximum)) {
        return duplicate(encaissement);
      }
    }
    return duplicate(table[0]);
  }


  /* -------------------------------------------- */
  static _evaluatePerte(formula, over20) {
    console.log("_evaluatePerte", formula, over20 )
    let perte = new Roll(formula, { over20:over20})
    perte.evaluate()
    return perte.total
  }
  
  /* -------------------------------------------- */
  static currentFatigueMalus( value, max)
  {
    max = (max < 16) ? 16 : max;
    max = (max > 30) ? 30 : max;
    value = (value > max*2) ? max*2 : value;
    value = (value < 0) ? 0 : value;
    
    let fatigueTab = fatigueMatrix[max];
    let fatigueRem = value;
    for (let idx=0; idx<fatigueTab.length; idx++) {
      fatigueRem -= fatigueTab[idx];
      if ( fatigueRem <= 0) {
        return fatigueMalus[idx];
      }
    }
    return -7; // This is the max !
  }
  
  /* -------------------------------------------- */  
  static findCompetence(compList, compName)  
  {
    compName = compName.toLowerCase();
    return compList.find(item => item.name.toLowerCase() == compName && (item.type =="competence" || item.type == "competencecreature"))
  }
  
  /* -------------------------------------------- */
  static getArmeCategory( arme )
  {
    if ( arme.data.competence == undefined) return 'competencecreature';

    let compname = arme.data.competence.toLowerCase(); 
    if ( compname.match("hache") ) return "hache";
    if ( compname.match("hast") ) return "hast";
    if ( compname.match("lance") ) return "lance";
    if ( compname.match("bouclier") ) return "bouclier";
    if ( compname.match("masse") ) return "masse";
    if ( compname.match("fléau") ) return "fleau";
    if ( compname.match("epée") ) { 
      let armename = arme.name.toLowerCase();
      if (armename == "dague" || armename.match("gnome") )
        return "epee_courte";
    }
    return "epee_longue";
  }
  
  /* -------------------------------------------- */
  static isArmeMelee( compName) 
  {
    let comp = compName.toLowerCase();
    if (comp.match("epée") || comp.match("épée") || comp.match("hache") || comp.match("fleau") || comp.match("mass") || comp.match("lance") || comp.match("hast") || comp == "dague" || comp=="bouclier")
      return true;
    return false;
  }
  
  /* -------------------------------------------- */
  static buildDefenseChatCard( attacker, target, rollData )
  {
    console.log("Attacker.defense", attacker, target, target.actor.isToken, attacker.data._id, rollData.competence.data.categorie );
    let myTarget = target.actor;
    let defenseMsg = { title: "Défense en combat", 
                       content: "<strong>"+myTarget.name+"</strong> doit se défendre : <br><span class='chat-card-button-area'>" +
                                "<a class='chat-card-button' id='encaisser-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "'>Encaisser !</a></span>",                                                         
                       whisper: ChatMessage.getWhisperRecipients( myTarget.name ),
                       attackerid: attacker.data._id,
                       defenderTokenId: target.data._id,
                       rollMode: true
                      };
    
    if ( rollData.competence.data.categorie == 'melee' || rollData.competence.data.categorie == 'creature') { // Melee attack or creature
      let defenderArmes = [];
      for (const arme of myTarget.data.items) {
        if (arme.type == "arme" && this.isArmeMelee(arme.data.competence)) {
          defenderArmes.push( arme );
          defenseMsg.content +=  "<br><a class='chat-card-button' id='parer-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "' data-armeid='"+arme._id+"'>Parer avec " + arme.name + "</a></span>";
        }
        if (arme.type == "competencecreature" && arme.data.isparade ) {
          defenderArmes.push( arme );
          defenseMsg.content +=  "<br><a class='chat-card-button' id='parer-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "' data-armeid='"+arme._id+"'>Parer avec " + arme.name + "</a></span>";
        }
      }
      defenseMsg.content +=  "<br><a class='chat-card-button' id='esquiver-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "'>Esquiver</a></span>";
    }
    if ( rollData.competence.data.categorie == "tir" ) {
      for (const arme of myTarget.data.items) { // Bouclier for parry
        if ( arme.type == "arme" && arme.name.toLowerCase.match("bouclier") ) {
          defenderArmes.push( arme );
          defenseMsg.content +=  "<br><a class='chat-card-button' id='parer-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "' data-armeid='"+arme._id+"'>Parer avec " + arme.name + "</a></span>";
        }
      }
    }
    if ( rollData.competence.data.categorie == "lancer" ) {
      for (const arme of myTarget.data.items) { // Bouclier for parry  Dodge/Esquive
        if ( arme.type == "arme" && arme.name.toLowerCase.match("bouclier") ) {
          defenderArmes.push( arme );
          defenseMsg.content +=  "<br><a class='chat-card-button' id='parer-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "' data-armeid='"+arme._id+"'>Parer avec " + arme.name + "</a></span>";
        }
      }
      defenseMsg.content +=  "<br><a class='chat-card-button' id='esquiver-button' data-attackerid='"+attacker.data._id + "' data-defendertokenid='" + target.data._id + "'>Esquiver</a></span>";
    }
    
    defenseMsg.toSocket = true; // True per default for all players
    if (game.user.isGM) { // In GM case, only if target is a player
      defenseMsg.toSocket = myTarget.hasPlayerOwner;
    }

    return defenseMsg;
  }

  /* -------------------------------------------- */
  static performSocketMesssage( sockmsg ) 
  {
    console.log(">>>>> MSG RECV", sockmsg);
    switch(sockmsg.msg)    {
      case  "msg_encaisser":
        return RdDUtility._handleMsgEncaisser(sockmsg.data);
      case  "msg_defense" :
        return RdDUtility._handleMsgDefense(sockmsg.data);
      case "msg_gm_chat_message":
        return ChatUtility.handleGMChatMessage(sockmsg.data);
    }
  }

  /* -------------------------------------------- */
  static _handleMsgDefense(data) {
    let defenderToken = canvas.tokens.get(data.defenderTokenId);
    if (defenderToken) {
      if ((game.user.isGM && !defenderToken.actor.hasPlayerOwner) || (defenderToken.actor.hasPlayerOwner && (game.user.character.id == defenderToken.actor.data._id))) {
        console.log("User is pushing message...", game.user.name);
        game.system.rdd.rollDataHandler[data.attackerid] = duplicate(data.rollData);
        data.whisper = [game.user];
        data.blind = true;
        data.rollMode = "blindroll";
        ChatMessage.create(data);
      }
    }
  }

  /* -------------------------------------------- */
  static buildItemsClassification( items ) {
    let itemsByType = {};
    for (const item of items) {
      let list = itemsByType[item.type];
      if (!list) {
        list = [];
        itemsByType[item.type] = list;
      }
      list.push(item);
    }
    return itemsByType;
  }

  /* -------------------------------------------- */
  static rollInitiativeCompetence( combatantId, arme ) {
    const combatant = game.combat.getCombatant(combatantId);
    const actor = combatant.actor; 

    if ( arme.name == "Autre action") {
      game.combat.rollInitiative(combatantId, "1d6" );
    } else if ( arme.name == "Draconic") {
      game.combat.rollInitiative(combatantId, "1d6+200" );
    } else {
      let initOffset = 0;
      let caracForInit = 0;
      let competence = RdDUtility.findCompetence( combatant.actor.data.items, arme.data.competence);

      if ( actor.data.type == 'creature' ||  actor.data.type == 'entite') {
        caracForInit = competence.data.carac_value;
      } else {
          caracForInit = actor.data.data.carac[competence.data.defaut_carac].value;
          if (competence.data.categorie == "lancer" ) { // Offset de principe pour les armes de jet
            initOffset = 40;
          }
          if (competence.data.categorie == "tir" ) { // Offset de principe pour les armes de jet
            initOffset = 80;
          }
      }
      // Cas des créatures et entités vs personnages
      let rollFormula =  "1d6+" + competence.data.niveau + "+" + Math.ceil(caracForInit/2) + "+" + initOffset;
      game.combat.rollInitiative(combatantId, rollFormula );
    }
  }

  /* -------------------------------------------- */
  static displayInitiativeMenu( html, combatantId) {
    // Recupération du combatant et de l'acteur associé
    const combatant = game.combat.getCombatant(combatantId);
    const actor = combatant.actor; 
    //console.log("Combattant : ", combatant);

    let armesList = [];
    if ( actor.data.type == 'creature' ||  actor.data.type == 'entite') {
      for (const competenceItem of actor.data.items) {
        if ( competenceItem.data.iscombat) { // Siule un item de type arme          
          armesList.push( { name: competenceItem.name, data: { niveau: competenceItem.data.niveau, competence: competenceItem.name } } );
        }
      }
    } else {
      // Recupération des items 'arme'
      let itemsByType = RdDUtility.buildItemsClassification( combatant.actor.data.items );
      armesList = itemsByType['arme'];
      // Force corps à corps et Draconic
      let cc = RdDUtility.findCompetence( combatant.actor.data.items, "Corps à corps");
      armesList.push( { name: "Corps à corps", data: { niveau: cc.data.niveau, description: "", force: 6, competence: "Corps à corps", dommages: combatant.actor.data.data.attributs.plusdom.value } } );
      armesList.push( { name: "Draconic", data: {  competence: "Draconic" } } );
    }
    armesList.push( { name: "Autre action", data: {  competence: "Autre action" } } );
    // Build the relevant submenu
    if ( armesList ) { 
      let menuItems = [];
      for ( let arme of armesList ) {
        menuItems.push( { 
          name: arme.data.competence, 
          icon: "<i class='fas fa-dice-d6'></i>",
          callback: target => { RdDUtility.rollInitiativeCompetence( combatantId, arme ) } } );
      }
      new ContextMenu(html, ".directory-list", menuItems ).render(); 
    }
  }

  /* -------------------------------------------- */
  static pushInitiativeOptions( html, options ) {
    options.push(
      {
        name: "Sélectionner l'initiative...",
        condition: true,
        icon: '<i class="far fa-question-circle"></i>',
        callback: target => { 
          RdDUtility.displayInitiativeMenu( html, target.data('combatant-id') );
        }
    });
  }

  /* -------------------------------------------- */
  static _handleMsgEncaisser(data) {
    if (game.user.isGM) { // Seul le GM effectue l'encaissement sur la fiche
      let rollData = game.system.rdd.rollDataHandler[data.attackerid]; // Retrieve the rolldata from the store
      let defenderToken = canvas.tokens.get(data.defenderTokenId);
      defenderToken.actor.encaisserDommages(rollData);
    }
  }

  /* -------------------------------------------- */
  static async chatListeners( html )
  {
    html.on("click", '#encaisser-button', event => {
      event.preventDefault();
      let attackerid = event.currentTarget.attributes['data-attackerid'].value;
      let defenderTokenId = event.currentTarget.attributes['data-defendertokenid'].value;
      if ( game.user.isGM ) { // Current user is the GM -> direct access
        let rollData = game.system.rdd.rollDataHandler[attackerid];
        rollData.attackerid = attackerid;
        rollData.defenderTokenId = defenderTokenId;
        let defenderToken = canvas.tokens.get( defenderTokenId );
        defenderToken.actor.encaisserDommages( rollData );
      } else { // Emit message for GM
        game.socket.emit("system.foundryvtt-reve-de-dragon", {
              msg: "msg_encaisser",
              data: { attackerid: attackerid, defenderTokenId: defenderTokenId  } 
          } );
      }
    });
    
    html.on("click", '#parer-button', event => {
      event.preventDefault();
      let attackerid = event.currentTarget.attributes['data-attackerid'].value;
      let defenderToken = canvas.tokens.get(event.currentTarget.attributes['data-defenderTokenId'].value );
      let armeId = event.currentTarget.attributes['data-armeid'].value;
      let rollData = game.system.rdd.rollDataHandler[attackerid];
      defenderToken.actor.parerAttaque( rollData, armeId );
    }); 

    html.on("click", '#esquiver-button', event => {
      event.preventDefault();
      let attackerid = event.currentTarget.attributes['data-attackerid'].value;
      let defenderToken = canvas.tokens.get(event.currentTarget.attributes['data-defenderTokenId'].value );
      let rollData = game.system.rdd.rollDataHandler[attackerid];
      //console.log("Esquive !", rollData, defenderActor);
      defenderToken.actor.esquiverAttaque( rollData );
    }); 

    html.on("click", '#particuliere-attaque', event => {
      event.preventDefault();
      let attackerid = event.currentTarget.attributes['data-attackerid'].value;
      let attackerActor = game.actors.get(event.currentTarget.attributes['data-attackerid'].value );
      let rollData = game.system.rdd.rollDataHandler[attackerid];
      rollData.particuliereAttaque = game.actors.get(event.currentTarget.attributes['data-mode'].value );
      //console.log("Particulère !", rollData);
      attackerActor.continueRoll( rollData );
    }); 

    html.on("click", '.tmr-passeur-coord a', event => {
      let coord   = event.currentTarget.attributes['data-tmr-coord'].value;
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let actor = game.actors.get( actorId );
      actor.tmrApp.forceDemiRevePosition(coord);
    });
     
  }

  /* -------------------------------------------- */
  /* Display help for /table */
  static displayHelpTable( msg )
  {
    msg.content = "";
    for (let [name, tableData] of Object.entries(table2func)) {
      msg.content += "<br>" + tableData.descr;
    }
    ChatMessage.create( msg );
  }

  /* -------------------------------------------- */
  /* Manage chat commands */
  static  processChatCommand( commands, content, msg ) {    
    // Setup new message's visibility
    let rollMode = game.settings.get("core", "rollMode");
    if (["gmroll", "blindroll"].includes(rollMode)) msg["whisper"] = ChatMessage.getWhisperRecipients("GM");
    if (rollMode === "blindroll") msg["blind"] = true;
    msg["type"] = 0;

    let command = commands[0];

    // Roll on a table
    if (command === "/table") {
      if ( commands[1] ) {
        let tableName = commands[1].toLowerCase();
        table2func[tableName].func();
      } else { 
        this.displayHelpTable( msg );
      }
      return false
    } else if (command === "/tmrr") {
        TMRUtility.getRencontre(commands[1], commands[2] )
        return false
    } else if (command === "/tmra") {
        TMRUtility.getTMRAleatoire( )
        return false
    }

    return true;
  }
}
