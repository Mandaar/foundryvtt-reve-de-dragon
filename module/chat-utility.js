
/**
 * Class providing helper methods to get the list of users, and 
 */
export class ChatUtility {

  static chatWithRollMode(chatOptions, name) {
    let rollMode = game.settings.get("core", "rollMode");
    chatOptions.user = game.user._id;
    switch (rollMode) {
      case "blindroll": // GM only
        if (!game.user.isGM) {
          ChatUtility.blindMessageToGM(chatOptions);

          chatOptions.whisper = [game.user._id];
          chatOptions.content = "Message envoyé en aveugle au Gardien";
        }
        else {
          chatOptions.whisper = ChatUtility.getUsers(user => user.isGM);
        }
        break;
      case "gmroll": // GM + rolling player
        chatOptions.user = game.user._id;
        chatOptions.whisper = ChatUtility.getWhisperRecipientsAndGMs(name);
        break;
      case "selfroll": // only the user
        chatOptions.user = game.user._id;
        chatOptions.whisper = [game.user._id];
        break;
      default:
      case "roll": // everybody
        chatOptions.whisper = undefined;
        break;
    }

    console.log("roll message", chatOptions);
    ChatMessage.create(chatOptions);
  }

  static getWhisperRecipientsAndGMs(name) {
    return ChatMessage.getWhisperRecipients(name)
      .concat(this.getUsers(user => user.isGM));
  }

  static getUsers(filter) {
    return game.users.filter(filter).map(user => user.data._id);
  }

  static blindMessageToGM(chatOptions) {
    let chatGM = duplicate(chatOptions);
    chatGM.whisper = ChatUtility.getUsers(user => user.isGM);
    chatGM.content = "Message aveugle de " + game.user.name + "<br>" + chatOptions.content;
    console.log("blindMessageToGM", chatGM);
    game.socket.emit("system.foundryvtt-reve-de-dragon", { msg: "msg_gm_chat_message", data: chatGM });
  }

  static handleGMChatMessage(data) {
    console.log("blindMessageToGM", data);
    if (game.user.isGM) { // message privé pour GM only
      data.user = game.user._id;
      ChatMessage.create(data);
    }
  }

}
