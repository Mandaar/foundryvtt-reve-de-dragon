/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class RdDItemSheet extends ItemSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
			classes: ["foundryvtt-reve-de-dragon", "sheet", "item"],
			template: "systems/foundryvtt-reve-de-dragon/templates/item-sheet.html",
			width: 520,
			height: 480
      //tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
		});
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;
    
    // Select competence categorie
    html.find("#categorie").on("click", this._onClickSelectCategorie.bind(this) );
  }
  
  /* -------------------------------------------- */

  async _onClickSelectCategorie(event) {
    event.preventDefault();
    
    const category = event.currentTarget.value;
    let level = CONFIG.RDD.level_category[category];    
    this.object.data.data.base = level;
    $("#base").val( level ); 
  }

  /* -------------------------------------------- */

  get template()
  {
    let type = this.item.type;
    return `systems/foundryvtt-reve-de-dragon/templates/item-${type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) {

    return this.object.update(formData);
  }
}
