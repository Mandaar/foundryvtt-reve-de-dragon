/* -------------------------------------------- */
const heures = [ "Vaisseau", "Sirène", "Faucon", "Couronne", "Dragon", "Epées", "Lyre", "Serpent", "Poisson Acrobate", "Araignée", "Roseau", "Château Dormant" ]

/* -------------------------------------------- */
export class RdDCalendrier extends Application {
    data = {
        saisons: [],
    };
    
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.template = "systems/foundryvtt-reve-de-dragon/templates/calendar-template.html";
        options.popOut = false;
        options.resizable = false;
        return options;
    }
    
    /*getData() {
        return templateData;
    }*/

}