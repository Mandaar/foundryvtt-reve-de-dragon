
/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { RdDUtility } from "./rdd-utility.js";

/* -------------------------------------------- */  
export class RdDActorHumanoideSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["rdd", "sheet", "actor"],
  	  template: "systems/foundryvtt-reve-de-dragon/templates/actor-humanoide-sheet.html",
      width: 640,
      height: 720,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac"}],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */
  getData() {
    let data = super.getData();
    
    data.itemsByType = {};
    for (const item of data.items) {
      let list = data.itemsByType[item.type];
      if (!list) {
        list = [];
        data.itemsByType[item.type] = list;
      }
      list.push(item);
    }

    // Compute current carac sum
    let sum = 0;
    Object.values(data.data.carac).forEach(carac => { if (!carac.derivee) { sum += parseInt(carac.value) } } );
    data.data.caracSum = sum;
    
    data.data.carac.taille.isTaille = true; // To avoid button link;
    data.data.nbLegeres   = this.actor.countBlessures(data.data.blessures.legeres.liste );
    data.data.nbGraves    = this.actor.countBlessures(data.data.blessures.graves.liste );
    data.data.nbCritiques = this.actor.countBlessures(data.data.blessures.critiques.liste );
    data.data.competencecreature = data.itemsByType["competencecreature"];

    RdDUtility.filterItemsPerTypeForSheet(data );
    RdDUtility.buildArbreDeConteneur( this, data );
    
    return data;
  }

  /* -------------------------------------------- */
  async _onDrop(event) {
    RdDUtility.processItemDropEvent(this, event);
    super._onDrop(event);
  }
  
  /* -------------------------------------------- */
  /** @override */
	activateListeners(html) {
    super.activateListeners(html);
    
    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Blessure control
    html.find('.blessure-control').click(ev => {
      const li   = $(ev.currentTarget).parents(".item");
      let btype  = li.data("blessure-type");
      let index  = li.data('blessure-index');
      let active = $(ev.currentTarget).data('blessure-active');
      //console.log(btype, index, active);
      this.actor.manageBlessureFromSheet(btype, index, active).then( this.render(true) );
    });

    // Blessure data
    html.find('.blessures-soins').change(ev => {
      const li   = $(ev.currentTarget).parents(".item");
      let btype  = li.data('blessure-type');
      let index  = li.data('blessure-index');
      let psoins    = li.find('input[name=premiers_soins]').val();
      let pcomplets = li.find('input[name=soins_complets]').val();
      let jours     = li.find('input[name=jours]').val();
      let loc       = li.find('input[name=localisation]').val();
      //console.log(btype, index, psoins, pcomplets, jours, loc);
      this.actor.setDataBlessureFromSheet(btype, index, psoins, pcomplets, jours, loc).then( this.render(true) );
    });

    // Roll Carac
    html.find('.carac-label a').click((event) => {
      let caracName = event.currentTarget.attributes.name.value;
      this.actor.rollCarac( caracName.toLowerCase() );
    });
    
    // On competence change
    html.find('.creature-carac').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCreatureCompetence( compName, "carac_value", parseInt(event.target.value) );
      } );    
    html.find('.creature-niveau').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCreatureCompetence( compName, "niveau", parseInt(event.target.value) );
      } );    
      html.find('.creature-dommages').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCreatureCompetence( compName, "dommages", parseInt(event.target.value) );
      } );    
  
    // Roll Skill
    html.find('.competence-label a').click((event) => {
      let compName = event.currentTarget.text;
      this.actor.rollCompetence( compName );
    });

    html.find('#vie-plus').click((event) => {
      this.actor.santeIncDec("vie", 1);
      this.render(true);
    });
    html.find('#vie-moins').click((event) => {
      this.actor.santeIncDec("vie", -1);
      this.render(true);
    });
    html.find('#endurance-plus').click((event) => {
      this.actor.santeIncDec("endurance", 1);
      this.render(true);
    });
    html.find('#endurance-moins').click((event) => {
      this.actor.santeIncDec("endurance", -1);
      this.render(true);
    });
  }
  

  /* -------------------------------------------- */

  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
